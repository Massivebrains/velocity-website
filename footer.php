 <footer class="osr-footer-2">
    <div class="container">
        <div class="row">
            <div data-aos="flip-up" data-aos-delay="200" class="col-md-6 col-sm-6">

                <div class="widget widget_about">
                    <div class="widget-title mb-28">
                        <h5 class="title"><img src="images/logo-white.png" alt="Velocity"></h5>
                    </div>
                    <div class="text newsletter-widget">
                        <p>Velocity will help you simplify your delivery needs, so you can focus on your core business.</p>

                        <div id="mc_embed_signup" class="input-field nl-form-container clearfix">
                            <form action="#" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="newsletterform validate" novalidate>
                                <input  type="email"  maxlength="32" placeholder="Enter your Email" class="email form-control nl-email-input" required>
                                <label class="search-icon p-absolute"><input class="btn th-bdr" id="mc-embedded-subscribe" type="submit" name="subscribe" value="Subscribe"></label>
                            </form>
                            <div id="notification_container"></div>
                        </div>

                    </div>
                </div>

            </div>
            <div data-aos="flip-up" data-aos-delay="300" class="col-md-3 col-sm-3">

                <div class="widget widget_contact">
                    <div class="widget-title">
                        <h5 class="title th-cl">Contact Us</h5>
                    </div>
                    <ul class="contac-list">
                        <li><i class="fa fa-phone"></i><a href="#">+234 807 766 6455</a></li>
                        <li><i class="fa fa-envelope"></i><a href="#">hello@velocity.ng</a></li>
                        <li>
                            <i class="fa fa-map-marker"></i>
                            <a href="#">
                                No 17a, June 12 Boulevard Road<br>
                                Abraham Adesanya Estate<br>
                                Lekki Ajah, Lagos NG
                            </a>
                        </li>
                    </ul>
                </div>

            </div>

            <div data-aos="flip-up" data-aos-delay="300" class="col-md-3 col-sm-3">

                <div class="widget widget_contact">
                    <div class="widget-title">
                        <h5 class="title th-cl">Helpful Links</h5>
                    </div>
                    <ul class="contac-list">
                        <li><i class="fa fa-link"></i><a href="privacy-policy.pdf" target="_blank">Privacy Policy</a></li>
                        <li><i class="fa fa-link"></i><a href="shipping-policy.pd">Shipping Policy</a></li>
                        <!-- <li><i class="fa fa-link"></i><a href="terms.pd">App Terms and Conditions</a></li> -->
                    </ul>
                </div>

            </div>

        </div>

        <div class="copy-right">
            <div class="pull-left">
                <p>Copyright @<?=date('Y') ?></p>
            </div>
            <div class="pull-right">
                <ul class="social-list long">
                    <li><a href="https://facebook.com/velocityng"><i class="fab fa-facebook-f"></i></a></li>
                    <li><a href="https://twitter.com/velocityng"><i class="fab fa-twitter"></i></a></li>
                    <li><a href="https://pintrest.com/velocityng"><i class="fab fa-pinterest-p"></i></a></li>
                    <li><a href="https://velocity.ng"><i class="fas fa-map-marker-alt"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>