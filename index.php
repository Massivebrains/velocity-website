<!DOCTYPE html>
<html lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <meta name="robots" content="index, follow" > 
    <meta name="theme-color" content="#4b6fff">
    <title>Home | Velocity</title>

    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/typography.css" rel="stylesheet">   
    <link href="css/animate.css" rel="stylesheet">
    <link href="css/aos.css" rel="stylesheet">
    <link href="css/svg.css" rel="stylesheet">
    <link href="css/slick.css" rel="stylesheet">
    <link rel="stylesheet" href="css/responsivemenu.css">
    <link href="css/widget.css" rel="stylesheet">
    <link href="css/shortcode.css" rel="stylesheet">
    <link href="style.css" rel="stylesheet">
    <link href="css/color.css" rel="stylesheet">
    <link href="css/responsive.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Ubuntu" rel="stylesheet">
    <link rel="shortcut icon" href="images/logo.png">
</head>
<body>

    <div id="home" class="main-wrapper">

        <?php include('header.php') ?>

        <?php include('home.php') ?>

        <div id="main-contant" class="main-contant">

         <?php include('about.php') ?>
         <?php include('faq.php') ?>

     </div>

     <?php include('footer.php') ?>
 </div>

 <script src="js/jquery.js"></script>
 <script src="js/bootstrap.js"></script>
 <script src="js/responsivemenu.js"></script>
 <script src="js/jquery.nav.js"></script>
 <script src="js/pie.js"></script>
 <script src="js/slick.min.js"></script>
 <script src="js/aos.js"></script>
 <script src="js/fontawesome.js"></script>
 <script src="js/jquery.validate.min.js"></script>
 <script src="js/contact-form-validation.min.js"></script>
 <script src="js/custom.js"></script>
</body>
</html>
