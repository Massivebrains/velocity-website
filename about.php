<style type="text/css">
    .box-icon-1{

        display:block !important;
    }

    .icon{

        width: 50%;
        height: 50%;
    }

    .text{

        font-size:20px;
        margin-top: 5px;
    }
</style>
<section class="border-bottom" id="about-us">
    <div class="container">
        <div class="row">
            <div data-aos="fade-left" data-aos-delay="300" class="col-md-7">
                <div class="aboutus-caption">
                    <h5 class="tab-fancy-heading th-bdr mb-35">About Us</h5>
                    <p>
                        Velocity offers small, medium and large businesses fast, efficient and affordable on- demand delivery service. We partner with your business to provide recurrent delivery services to your customers whenever you need it. 
                    </p>
                    <p class="c-hidden">
                        Velocity will help you simplify your delivery needs, so you can focus on your core business. 
                    </p>
                    <h5 class="tab-fancy-heading th-bdr mb-35">Why Velocity</h5>
                    <div class="row">
                        <div data-aos="fade-up" data-aos-delay="350" class="col-md-6 col-sm-6">

                            <div class="box-icon-1 radius-5 mb-50">

                                <img src="images/velocity/new/fast_and_reliable.png" class="icon">
                                <div  class="box-icon-text text">
                                    Fast Delivery Service
                                </div>

                            </div>

                        </div>

                        <div data-aos="fade-up" data-aos-delay="400" class="col-md-6 col-sm-6">

                            <div class="box-icon-1 radius-5 mb-50">

                                <img src="images/velocity/new/rider_monitor.png" class="icon">
                                <div class="box-icon-text text">
                                    Trusted Riders
                                </div>

                            </div>

                        </div>
                        <div data-aos="fade-up" data-aos-delay="500" class="col-md-6 col-sm-6">

                            <div class="box-icon-1 radius-5 mb-30">

                                <img src="images/velocity/new/eco.png" class="icon">
                                <div class="box-icon-text text">
                                    Exceptional Customer Service
                                </div>

                            </div>

                        </div>
                        <div data-aos="fade-up" data-aos-delay="500" class="col-md-6 col-sm-6">

                            <div class="box-icon-1 radius-5 mb-30">

                                <img src="images/velocity/new/professional_riders.png" class="icon">

                                <div class="box-icon-text text">
                                    Professional Riders
                                </div>

                            </div>

                        </div>


                    </div>
                </div>
            </div>
            <div data-aos="fade-right" data-aos-delay="300" class="col-md-5">

                <div class="thumb aboutus-thumb mb-30">
                    <img src="images/second.png" style="width: 600px; height: 354px" alt="Velocity">
                </div>

            </div>
        </div>
    </div>
</section>