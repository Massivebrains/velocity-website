<header id="header" class="oscar-header-4 sticky-header">

    <div class="nav-outer">
        <div class="container">

            <h1 class="logo">
                <a href="index.php" title="Velocity">
                    <img src="images/logo.png" alt="Velocity">
                </a>
            </h1>

            <div class="pull-right menu-holder">
                <div class="mobile-menu"></div>
                <div class="main-menu navigation">
                    <nav>
                        <ul id="opg-nav">
                            <li class="active"><a href="#home">Home</a></li>                                                         
                            <li><a href="#about-us">About</a></li>
                            <li><a href="#featured">FAQ</a></li>
                            <li><a href="#contact">Register Your Business</a></li>
                        </ul>
                    </nav>
                </div>
            </div> 
        </div>
    </div>

</header>
