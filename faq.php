 <section class="border-bottom" data-aos="fade-up" data-aos-delay="300">
    <div class="container">

        <div class="row">

            <div class="col-md-5" id="featured">
                <div class="section-heading-2">
                    <h4 class="title">Frequently Asked <span class="th-cl">Questions</span></h4>
                </div>

                <div id="oscar-tab-wrap" class="oscar-tab-wrap">

                    <ul>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">How do I schedule my deliveries with Velocity?</h4>
                            <p>Simple. If you are a Business Client, register your business and tell us how often you want us to pick up and deliver to your customers. Fill registration form by your left. If you only need a one time service, please call our delivery service number +234 807 766 6455 or send an email to hello@velocity.ng</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">Are my items safe with your riders?</h4>
                            <p>We only recruit professional riders. In addition, all our riders, when joining Velocity undergo security clearance checks and training. We are always improving our service and so our riders are constantly trained and evaluated to ensure we live up to the Velocity promise of safety and security.</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">How does Velocity calculate delivery prices?</h4>
                            <p>Our pricing is based on distance and the size of the item you want to deliver as well as the frequency. For Business Clients, our fees are charged on a monthly retainership. Register your business to get started.</p>
                        </li>

                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">How do I pay?</h4>
                            <p>Simple. You can make a direct transfer to our bank account or pay securely via <a href="https://paystack.com" target="_blank">Paystack</a>. You can also call +234 8077666455 to request for a Paystack payment link. Please see our payment details <a href="#">here</a>.</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">What Items do you deliver?</h4>
                            <p>Please see our shipping policy <a href="#">here</a>.</p>
                        </li>

                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">Do you have an insurance cover?</h4>
                            <p>Yes, all items delivered by Velocity are fully insured against theft and damages</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">How do I ensure a delivery goes to the right person?</h4>
                            <p>When registering as a business on our platform, you can give us specific instructions about your deliveries. You can also send us additional information or instructions about your delivery (if you have any changes) 48 hours before it is executed.  Our Riders have been trained to request for additional information if necessary before they deliver an item to the recipient.</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px">What locations do you serve?</h4>
                            <p>We are starting out with Lagos Mainland and Island. Never mind, our operations will get to your area soon.</p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px"></h4>
                            <p></p>
                        </li>
                        <li style="margin-bottom:20px;">
                            <h4 style="margin-bottom:5px"></h4>
                            <p>
                                <div class="alert alert-info">
                                    Can't find the answer to your question? Please contact us, we're always happy to help.
                                </div>
                            </p>
                        </li>
                    </ul>

                </div>
            </div>

            <div class="col-md-7" id="contact">
                <iframe src="https://docs.google.com/forms/d/e/1FAIpQLSdBJb4jzOe90oKH-PFjmR4I3s2uQgQVW3fRHk34fe7jrC6zQQ/viewform?embedded=true" width="1000" height="2050" frameborder="0" marginheight="0" marginwidth="0">Loading...</iframe>
            </div>
        </div>
    </div>
</section>