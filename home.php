<div class="main-banner static-main-banner gray-bg">
    <div class="container">
        <div class="row">
            <div data-aos="fade-right" class="col-md-7 col-sm-7">
                <div class="banner-caption-wrap">
                    <h1 class="banner-small-title">Welcome to Velocity!</h1>
                    <h1 class="banner-title">Lightning <span class="th-cl">Fast & Reliable</span> Pickup and delivery services at your convenience</h1>
                    <!-- <p class="banner-caption">I’ve a dedicated team of Qualified Professionals with industry experience and technical expertise to manage a multitude of tasks.</p> -->
                    <a href="#about-us" class="btn th-bg btn-sm">Learn More</a>
                    <a href="#contact" class="btn th-bdr hidden-sm hidden-xs">Register Your Business</a>
                </div>
            </div>
            <div data-aos="fade-left" class="col-md-5 hidden-xs col-sm-5">
                <div class="banner-thumb">
                    <img src="images/first.png" style="width: 450px; height: 622px" alt="Velocity">
                </div>
            </div>
        </div>
    </div>
</div>
